package com.alexia.garcia.convertidoros;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alexia.garcia.convertidoros.databinding.ActivityMainBinding;

public class MainActivity extends Activity {

    public double miles,km;
    public String result, miles_string;

    TextView result_tv= (TextView)findViewById(R.id.tv_km);
    EditText miles_et =(EditText)findViewById(R.id.ed_miles);
    Button result_btn =(Button)findViewById(R.id.btn_convert);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result_btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                convertMiles();
            }

        });

        //setAmbientEnabled();
    }
    public void convertMiles(){
        //1 miles =1.600km
        miles_string=miles_et.getEditableText().toString();
        miles=Double.parseDouble(miles_string);
        km=miles*1609;
        result=km+" KM";
        result_tv.setText(result);

    }

}